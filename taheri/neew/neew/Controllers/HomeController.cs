﻿using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Infrastructure;
using CaptchaMvc.Models;
using neew.Models;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
//using Microsoft.AspNet.Identity.EntityFramework;
namespace neew.Controllers
{
    public class HomeController : Controller
    {
        private DataBaseContext db = new DataBaseContext();
        private LoginDBEntities1 db2 = new LoginDBEntities1();

        // GET: /Home/
        public ActionResult Index()
        {
            return View(db.users.ToList());
        }

        public ActionResult BookDisplay()
        {

            return View(db.books.ToList());
        }

        [HttpGet]
        public ActionResult Shop(int? id)
        {

            Book book = db.books.Find(id);
            return View(book);
        }
        //.........................................basket
        [HttpGet]
        public ActionResult Basket(int? id)
        {
            string name = User.Identity.Name;
            if (id != null)//کتاب را انتخاب کرده و وارد سبد شده
            {
                Book book = db.books.Find(id);
                int price = book.Price;

                var finall = db.Fctusrs.FirstOrDefault(x => x.OwnerName == name && x.Final == false);
                if (finall != null)//کسی ک سبد خرید داره
                {
                    int ftid = db.Fctusrs.Where(x => x.OwnerName == name && x.Final == false).Select(x => x.FID).Single();
                    var was = db.Fctboks.FirstOrDefault(x => x.FID == ftid && x.BookId == id);
                    if (was == null)//کتاب قبلا انتخاب نشده باشد
                    {

                        FctBok fbook = new FctBok();
                        fbook.FID = db.Fctusrs.Where(x => x.OwnerName == name && x.Final == false).Select(x => x.FID).Single();
                        Book bk = db.books.Find(id);
                        fbook.BookId = bk.IDBook;
                        fbook.BName = bk.BName;
                        fbook.Price = bk.Price;
                        fbook.was = true;
                        db.Fctboks.Add(fbook);
                        db.SaveChanges();

                        FctUsr fuser = db.Fctusrs.FirstOrDefault(x => x.OwnerName == name && x.Final == false);
                        int pri = fuser.TotalPrice + price;
                        fuser.TotalPrice = pri;
                        fuser.Count = db.Fctboks.Where(x => x.FID == ftid).Count();
                        db.Entry(fuser).State = EntityState.Modified;
                        db.SaveChanges();

                    }

                    return View(db.Fctboks.Where(m => m.FID == ftid).ToList());
                }
                if (finall == null)//کسی ک سبد خرید نداره
                {
                    FctUsr fuser = new FctUsr();
                    fuser.OwnerName = User.Identity.Name;
                    fuser.TotalPrice = price;
                    fuser.Final = false;
                    fuser.Count = 1;
                    db.Fctusrs.Add(fuser);
                    db.SaveChanges();

                    //factor ketab
                    FctBok fbook = new FctBok();
                    fbook.FID = fuser.FID;
                    Book bk = db.books.Find(id);
                    fbook.BookId = bk.IDBook;
                    fbook.was = true;
                    fbook.BName = bk.BName;
                    fbook.Price = bk.Price;
                    db.Fctboks.Add(fbook);
                    db.SaveChanges();
                    //واسه نمایش سبد خرید
                    int ftid = db.Fctusrs.Where(x => x.OwnerName == name && x.Final == false).Select(x => x.FID).Single();
                    return View(db.Fctboks.Where(m => m.FID == ftid).ToList());
                }
            }
            else//مشاهده سبد خرید را زده
            {
                var has = db.Fctusrs.FirstOrDefault(x => x.OwnerName == name && x.Final == false);
                if (has != null)
                {
                    ViewBag.has = null;
                    int ftid = db.Fctusrs.Where(x => x.OwnerName == name && x.Final == false).Select(x => x.FID).Single();
                    return View(db.Fctboks.Where(m => m.FID == ftid).ToList());
                }
                else//سبد خرید خالی است
                {
                    ViewBag.em = "okk";
                    return View("Basket");
                }

            }
            return View();
        }

        [HttpGet]
        public ActionResult Empty()
        {
            return View();
        }
        //..........................................final
        [HttpGet]
        public ActionResult Final()
        {
            string name = User.Identity.Name;
            FctUsr fuser = db.Fctusrs.FirstOrDefault(x => x.OwnerName == name && x.Final == false);
            fuser.Final = true;
            db.Entry(fuser).State = EntityState.Modified;
            db.SaveChanges();

            int fidd = fuser.FID;
            int count = fuser.Count;
            for (int i = 0; i < count; i++)//موجود بودن کتابهارو ناموجود میکنه
            {
                FctBok bok1 = db.Fctboks.FirstOrDefault(x => x.FID == fidd && x.was == true);
                int bid = bok1.BookId;

                Book bok = db.books.Where(x => x.IDBook == bid).Single();
                bok.Existence = false;
                db.Entry(bok).State = EntityState.Modified;
                db.SaveChanges();
                bok1.was = false;
                db.Entry(bok1).State = EntityState.Modified;
                db.SaveChanges();
                int a = db.Fctboks.Where(x => x.BName == bok1.BName && x.was == true).Count();
                for (int j = 0; j < count; j++)
                {
                    FctBok bok2 = db.Fctboks.FirstOrDefault(x => x.BName == bok1.BName && x.was == true);
                    if (bok2 != null)
                    {
                        db.Fctboks.Remove(bok2);
                        db.SaveChanges();
                    }
                }
            }
            return View();
        }
        //...................................deletebook
        public ActionResult DeleteB(int? id, int? id2)
        {

            int factor = db.Fctboks.Where(x => x.BookId == id && x.FID == id2).Select(x => x.id).Single();
            FctBok fbok = db.Fctboks.Find(factor);
            db.Fctboks.Remove(fbok);
            db.SaveChanges();
            FctUsr fu = db.Fctusrs.Find(id2);
            fu.Count = fu.Count - 1;
            if (fu.Count == 0)
            {
                db.Fctusrs.Remove(fu);

            }
            db.SaveChanges();
            return RedirectToAction("Basket");
        }
        //..........................................................register
        // GET: /Home/Create
        public ActionResult Create()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View("Index");
            }
            //ViewBag.CityID = new SelectList(db.cities, "CityID", "CityName");
            return View();
        }

        // POST: /Home/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[CaptchaValidation("CaptchaCode", "ExampleCaptcha", "Incorrect CAPTCHA code!")]
        public ActionResult Create([Bind(Include = "ID,UName,FName,LName,Email,Pass,Phone,City,Captcha")] User user)
        {
            if (ModelState.IsValid)
            {

                bool validunam = db.users.Any(x => x.UName == user.UName);
                bool validmail = db.users.Any(x => x.Email == user.Email);

                if (validunam)
                {
                    ViewBag.log = "نام کاربری شما تکراری است";
                    ///ViewBag.CityID = new SelectList(db.cities, "CityID", "CityName");
                    return View("Create");
                }
                if (validmail)
                {
                    ViewBag.log = "ایمیل شما تکراری است";
                    //ViewBag.CityID = new SelectList(db.cities, "CityID", "CityName");
                    return View("Create");
                }

                //return RedirectToAction("Captcha");
                if (ModelState["CaptchaInputText"] == null)
                {
                    if (this.IsCaptchaValid("Captcha is not valid"))
                    {
                        string passwrodSalt = user.Pass;
                        string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(passwrodSalt, "sha1");
                        db.users.Add(user);
                        user.Pass = hashedPwd;
                        db.SaveChanges();
                        LoginViewModel log = new LoginViewModel();
                        FormsAuthentication.SetAuthCookie(user.UName, log.Remember);

                        Person person = new Person();
                        person.UserID = user.ID;
                        person.UserName = user.UName;
                        person.Email = user.Email;
                        person.Password = user.Pass;


                        RoleUser ro = new RoleUser();
                        ro.UserID = person.UserID;
                        ro.RoleID = 2;
                        ro.RoleUserID = person.UserID;

                        db2.People.Add(person);
                        db2.RoleUsers.Add(ro);
                        db2.SaveChanges();


                        return RedirectToAction("Index");
                    }
                    if (!ModelState.IsValid)
                    {
                        //ViewBag.CityID = new SelectList(db.cities, "CityID", "CityName");
                        return View("Create");
                    }
                }
            }
            return View(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }

        //....................................................add book

        [HttpGet]
        //[Authorize(Roles="Admin")]
        public ActionResult AddBook()
        {
            return View("AddBook");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBook([Bind(Include = "IDBook,BName,Writer,Translator,Publisher,Year,Condition,Price,Editing,fc,file")] FormCollection fc1, HttpPostedFileBase file1, WaitBook wbook)
        {
            if (ModelState.IsValid)
            {

                wbook.BookOwner = User.Identity.Name;
                wbook.Existence = true;
                db.Wbook.Add(wbook);


                var allowedExtensions = new[] { ".Jpg", ".png", ".jpg", "jpeg" };

                if (file1 != null)
                {
                    var fileName = Path.GetFileName(file1.FileName);
                    var ext = Path.GetExtension(file1.FileName);
                    if (allowedExtensions.Contains(ext))
                    {

                        string name = Path.GetFileNameWithoutExtension(fileName);
                        string myfile = name + "_" + wbook.IDBook + ext;
                        var path = "~/img/uploads/" + myfile;

                        wbook.Image = path;
                        db.SaveChanges();
                        path = Path.Combine(Server.MapPath("~/img/uploads/"), myfile);
                        file1.SaveAs(path);
                    }
                    else
                    {
                        ViewBag.message = "لطفا فقط فایل تصویری انتخاب کنید";
                        return View("AddBook");
                    }
                    db.SaveChanges();
                    exist exist = new exist();
                    exist.IDBook = wbook.IDBook;
                    db.exists.Add(exist);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return View("book");
        }

        //...........................................Login
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel log)
        {
            string passwrodSalt = log.Pass;
            string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(passwrodSalt, "sha1");
            var person = db.users.FirstOrDefault(u => u.UName == log.UName && u.Pass == hashedPwd);
            if (ModelState["CaptchaInputText"] == null)
            {
                if (this.IsCaptchaValid("Captcha is not valid"))
                {
                    if (person != null)
                    {
                        FormsAuthentication.SetAuthCookie(log.UName, log.Remember);
                        //Person per=  db2.People.Where(x => x.UserName == log.UName).Single();
                        //RoleUser role= db2.RoleUsers.Where(x => x.UserID == per.UserID).Single();
                        // if(role.RoleID==1)
                        //  {
                        //      ViewBag.admin = "Ok";
                        //      return RedirectToAction("Index");

                        //  }
                        return RedirectToAction("Index");
                    }
                    ViewBag.error1 = "نام کاربری یا پسورد شما اشتباه است";
                    return View("Login");
                }
                if (!ModelState.IsValid)
                {
                    return View("Login");
                }
            }

            return View("Login");
        }
        //.....................................................log out
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        public ActionResult Shpic(int? id)
        {
            return View(db.books.Where(x => x.IDBook == id));
        }
        //..............................................profile
        [HttpGet]
        public ActionResult Profile()
        {
            string name = User.Identity.Name;
            int id = db.users.Where(x => x.UName == name).Select(x => x.ID).Single();
            User user = db.users.Find(id);
            return View(user);
        }
        [HttpGet]
        public ActionResult EditPass()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EditPass([Bind(Include = "Pass,NewPass,RNPass")] changePass chPass)
        {
            if (ModelState.IsValid)
            {
                string name = User.Identity.Name;
                int id = db.users.Where(x => x.UName == name).Select(x => x.ID).Single();
                User user = db.users.Find(id);
                string passwrodSalt = chPass.Pass;
                string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(passwrodSalt, "sha1");
                if (user.Pass == hashedPwd)//رمزدرسته
                {
                    if (chPass.NewPass == chPass.RNPass)
                    {
                        string passwrod = chPass.NewPass;
                        string newpass = FormsAuthentication.HashPasswordForStoringInConfigFile(passwrod, "sha1");
                        user.Pass = newpass;
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                        ViewBag.Change = "تغییر رمز با موفقیت انجام شد";
                        return View("EditPass");
                    }
                    else
                    {
                        ViewBag.Repeat = "تکراررمزجدید اشتباه است";
                        return View("EditPass");
                    }
                }
                else
                {
                    ViewBag.No = "رمز عبور شما اشتباه است";
                    return View("EditPass");
                }
            }
            if (!ModelState.IsValid)
            {
                return View("EditPass");
            }
            return View();
        }

        [HttpGet]
        public ActionResult EditUser()
        {
            string name = User.Identity.Name;
            int id = db.users.Where(x => x.UName == name).Select(x => x.ID).Single();
            User user = db.users.Find(id);
            return View(user);
        }
        [HttpPost]

        public ActionResult EditUser([Bind(Include = "ID,UName,FName,LName,Email,Psss,Phone,City,InsertTime")] User user)
        {

            if (ModelState.IsValid)
            {
                string name = User.Identity.Name;

                //if (name != user.UName)
                //{
                //    ViewBag.name = "شما قادر به تغییر نام کاربری نیستید";
                //    return View("EditUser");
                //}

                //else
                //{
                    string mail = db.users.Where(x => x.UName == name).Select(x => x.Email).Single();
                    if (mail != user.Email)
                    {
                        bool validmail = db.users.Any(x => x.Email == user.Email);
                        if (validmail)
                        {
                            ViewBag.log = "ایمیل شما تکراری است";
                            return View("EditUser");
                        }
                        else
                        {
                            db.Entry(user).State = EntityState.Modified;
                            db.SaveChanges();
                            return RedirectToAction("Profile");
                        }
                    }
                    else
                    {

                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Profile");

                    }
                //}
            }
            return View("EditUser");
        }


        public ActionResult MyBook()
        {
            string name = User.Identity.Name;
            return View(db.books.Where(x => x.BookOwner == name));
        }

        public ActionResult MyFactor()
        {
            string name = User.Identity.Name;
            return View(db.Fctusrs.Where(x => x.OwnerName == name));
        }
        public ActionResult FactorDetails(int? id)
        {
            return View(db.Fctboks.Where(x => x.FID == id));
        }
    }
}
