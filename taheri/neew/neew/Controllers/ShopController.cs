﻿using CaptchaMvc.Attributes;
using CaptchaMvc.Infrastructure;
using neew.Models;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq.Expressions;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Models;
using System.Web.Routing;
namespace neew.Controllers
{

    [Authorize(Roles="Admin")]
    public class ShopController : Controller
    {
        private DataBaseContext db = new DataBaseContext();
        private LoginDBEntities1 db2 = new LoginDBEntities1();
        public ActionResult Index()
        {
            return View();
            
        }

        [HttpGet]
        public ActionResult Show(int? id)
        {

            Book book = db.books.Find(id);
            return View(book);
        }

        public ActionResult User()
        {
            return View(db.users.Where(x=>x.UName!="fereshte").ToList());
        }
        public ActionResult DeleteUser(int? id)////from person
        {          
            User user = db.users.Find(id);           
            db.users.Remove(user);
            
            db.SaveChanges();
            return RedirectToAction("User");
        }
        public ActionResult Book()
        {
            return View(db.books.ToList());
        }

        public ActionResult DeleteBook(int? id)
        {
            Book book = db.books.Find(id);
            db.books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Book");
        }


        public ActionResult DeleteBookFac(int? id,int? id2)
        {
            int factor = db.Fctboks.Where(x => x.FID == id && x.BookId== id2).Select(x => x.id).Single();
            FctBok fbok = db.Fctboks.Find(factor);
            db.Fctboks.Remove(fbok);      
            FctUsr fu = db.Fctusrs.Find(id);
            fu.Count--; 
            if (fu.Count == 0)
            {
                db.Fctusrs.Remove(fu);

            }
            db.SaveChanges();
            return RedirectToAction("FactorDetails", new {id=id });
        }

       
        public ActionResult EditBook(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book= db.books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBook([Bind(Include = "IDBook,BName,Writer,Translator,Publisher,Year,Condition,Price,Editing,BookOwner,Existence,InsertTime")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Entry(book).State = EntityState.Modified;          
                db.SaveChanges();
                return RedirectToAction("Book");
            }
            return View("EditBook");
        }

   
        public ActionResult Seen(int? id)
        {

            WaitBook wbook = db.Wbook.Find(id);
            return View(wbook);
        }
        [HttpPost]
        public ActionResult Seen([Bind(Include = "IDBook,BName,Writer,Translator,Publisher,Year,Condition,Price,Editing,BookOwner,Existence,Image,InsertTime")] Book book)
        {
               
                WaitBook b = db.Wbook.Find(book.IDBook);
               
               

                book.Existence= true;
                book.Image = b.Image;
                db.books.Add(book); 
               db.Wbook.Remove(b);
               db.SaveChanges();
                //BookPic pic=db.pics.Where(x => x.BookID == b.IDBook).Single();
                //pic.BookID = book.IDBook;

                //db.Entry(pic).State = EntityState.Modified;
            
                db.SaveChanges();
                return RedirectToAction("NewBook");        
        }

        public ActionResult DeleteWB(int? id)
        {
           WaitBook b= db.Wbook.Find(id);
           db.Wbook.Remove(b);
           db.SaveChanges();
           return RedirectToAction("NewBook");
        }
       [HttpGet]
        public ActionResult NewBook()
        {
            return View(db.Wbook.ToList());
        }


       public ActionResult Factor()
       {
           return View(db.Fctusrs.Where(x => x.Final == true));

       }

       public ActionResult FactorDetails(int? id)
       {
           return View(db.Fctboks.Where(x => x.FID == id));
       }


       public ActionResult Edit(int? id)
       {
           //string name = User.Identity.Name;
           //int id= db.users.Where(x => x.UName == name).Select(x => x.ID).Single();
           if (id == null)
           {
               return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
           }
           User user = db.users.Find(id);
           if (user == null)
           {
               return HttpNotFound();
           }
           return View(user);
       }
       [HttpPost]
       [ValidateAntiForgeryToken]
       public ActionResult Edit([Bind(Include = "ID,UName,FName,LName,Email,Pass,Phone,City,InsertTime")] User user)
       {
           if (ModelState.IsValid)
           {
                string mail = db.users.Where(x => x.UName == user.UName).Select(x=>x.Email).Single();
                if (mail != user.Email)
                {
                    bool validmail = db.users.Any(x => x.Email == user.Email);
                    if (validmail)
                    {
                        ViewBag.log = "ایمیل شما تکراری است";
                        return View("Edit");
                    }
                    else
                    {
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("User");
                    }
                }
                else
                {
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("User");
                }
           }
           return View(user);
       }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
	}
}