﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using neew.Models;

namespace neew.Controllers
{
    public class EditUserController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: /EditUser/
        public ActionResult Index()
        {
            return View(db.users.ToList());
        }

        // GET: /EditUser/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: /EditUser/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /EditUser/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,UName,FName,LName,Email,Pass,Phone,City,InsertTime")] User user)
        {
            if (ModelState.IsValid)
            {
                db.users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: /EditUser/Edit/5
        public ActionResult Edit()
        {
            string name = User.Identity.Name;
            int id = db.users.Where(x => x.UName == name).Select(x => x.ID).Single();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: /EditUser/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,UName,FName,LName,Email,Pass,Phone,City,InsertTime")] User user)
        {
            //string name = User.Identity.Name;
            //int id = db.users.Where(x => x.UName == name).Select(x => x.ID).Single();
            //user.ID = id;
            if (ModelState.IsValid)
            {

                string mail = db.users.Where(x => x.UName == user.UName).Select(x=>x.Email).Single();
                if (mail != user.Email)
                {
                    bool validmail = db.users.Any(x => x.Email == user.Email);
                    if (validmail)
                    {
                        ViewBag.log = "ایمیل شما تکراری است";
                        return View("Edit");
                    }
                    else
                    {
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction(actionName: "Profile", controllerName: "Home");
                    }
                }
                else { 
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction(actionName:"Profile",controllerName:"Home");
                }
            }
            return View(user);
        }

        // GET: /EditUser/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: /EditUser/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.users.Find(id);
            db.users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
