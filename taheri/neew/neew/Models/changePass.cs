﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace neew.Models
{
    public class changePass
    {
        public changePass() { }

        [Required(ErrorMessage = "لطفا پسورد را وارد کنید")]
        public string Pass { set; get; }

        [Required(ErrorMessage = "لطفا پسوردجدید را وارد کنید")]
        public string NewPass { set; get; }

        [Required(ErrorMessage = "لطفا پسوردجدید را تکرار کنید")]
        public string RNPass { set; get; }
    }
}