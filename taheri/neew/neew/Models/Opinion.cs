﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace neew.Models
{
    public class Opinion
    {
        public Opinion() { }
        public Opinion(string email, string mytext)
        {
            this.Email = email;
            this.Mytext = mytext;
        }

        [System.ComponentModel.DataAnnotations.Key]
        [System.ComponentModel.DataAnnotations.Required]
        public int id { set; get; }

        [Required(ErrorMessage = "لطفا ایمیل خود را وارد کنید")]
        [RegularExpression(".+\\@.+\\..+",
            ErrorMessage = "لطفا ایمیل را صحیح وارد کنید")]
        public string Email { get; set; }

        public string Mytext { get; set; }

        public bool? ok { get; set; }
    }
}