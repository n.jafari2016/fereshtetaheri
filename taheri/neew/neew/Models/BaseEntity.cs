﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace neew.Models
{
    public class BaseEntity : System.Object
    {
        public BaseEntity()
        {
           
        }


        [System.ComponentModel.DataAnnotations.Key]
        [System.ComponentModel.DataAnnotations.Required]
        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated
            (System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        //پراپرتی آی دی
        public int ID { get; set; }
    }
}