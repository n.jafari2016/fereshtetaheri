﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace neew.Models
{
    public class FctBok 
    {
        public FctBok() { }

        [System.ComponentModel.DataAnnotations.Key]
        [System.ComponentModel.DataAnnotations.Required]
        public int id { set; get; }

        public int FID { get; set; }
        public int BookId { get; set; }
        public string BName { get; set; }
        public int Price { get; set; }
        public bool was { set; get; }
    }
}