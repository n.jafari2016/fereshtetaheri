﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace neew.Models
{
    public class DataBaseContext : System.Data.Entity.DbContext
    {
        static DataBaseContext()
        {
            System.Data.Entity.Database.SetInitializer(
                new System.Data.Entity.DropCreateDatabaseIfModelChanges<DataBaseContext>());
        }
        public DataBaseContext()
        {

        }
        public System.Data.Entity.DbSet<User> users { get; set; }
        public System.Data.Entity.DbSet<Book> books { get; set; }
        public System.Data.Entity.DbSet<City> cities { get; set; }
        public System.Data.Entity.DbSet<FctBok> Fctboks { get; set; }
        public System.Data.Entity.DbSet<FctUsr> Fctusrs { get; set; }
        public System.Data.Entity.DbSet<BookPic> pics { get; set; }
        public System.Data.Entity.DbSet<exist> exists { get; set; }
        public System.Data.Entity.DbSet<WaitBook> Wbook { get; set; }
        public System.Data.Entity.DbSet<Opinion> opinions { get; set; }

    }
}