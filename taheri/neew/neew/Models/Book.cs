﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace neew.Models
{
    public class Book :Date
    {

        public Book()
        {
          
        }
        public Book(string bname, string writer, string translator, string publisher, int year, string condition,
            int price, int editing)
        {
            this.BName = bname;
            this.Writer = writer;
            this.Translator = translator;
            this.Publisher = publisher;
            this.Year = year;
            this.Condition = condition;
            this.Price = price;
            this.Editing = editing;


        }

        /// </summary>
        [System.ComponentModel.DataAnnotations.Key]
        [System.ComponentModel.DataAnnotations.Required]
        [System.ComponentModel.DataAnnotations.Schema.Column("BookId")]
        //[System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated
        //    (System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int IDBook { get; set; }


        [Required(ErrorMessage = "لطفا نام کتاب را وارد کنید")]
        public string BName { get; set; }

        [Required(ErrorMessage = "لطفا نام نویسنده را وارد کنید")]
        public string Writer { get; set; }

        //[Required(ErrorMessage = "لطفا نام مترجم را وارد کنید")]
        public string Translator { get; set; }

        [Required(ErrorMessage = "لطفا نام انتشارات را وارد کنید")]
        public string Publisher { get; set; }


        [Required(ErrorMessage = "لطفا سال چاپ را وارد کنید")]
        public int Year { get; set; }


        [Required(ErrorMessage = "لطفا وضعیت کتاب را انتخاب کنید")]
        public string Condition { get; set; }

        [Required(ErrorMessage = "لطفا قیمت موردنظرتان را وارد کنید")]
        public int Price { get; set; }

        [Required(ErrorMessage = "لطفا شماره ویرایش را وارد کنید")]
        public int Editing { get; set; }

        public string BookOwner { get; set; }
        public bool? Existence { get; set; }
        public string Image { set; get; }
        public static Book GetDefault()
        {
            Book obook =
                new Book();

            obook.Writer = "Undefined!";

            return obook;
        }
    }
}