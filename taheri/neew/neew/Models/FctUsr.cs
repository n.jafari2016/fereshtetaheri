﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace neew.Models
{
    public class FctUsr : Date
    {
        public FctUsr() { }

           public FctUsr(string Ownername, bool final, int total,int count)
        {
            this.OwnerName = Ownername;
            this.Final = final;
            this.TotalPrice = total;
            this.Count = count;
        }


        [System.ComponentModel.DataAnnotations.Key]
        [System.ComponentModel.DataAnnotations.Required]
        public int FID { get; set; }
        public string OwnerName { set; get; }
        public bool Final { set; get; }

        public int TotalPrice { set; get; }
        public int Count { get; set; }

        public static FctUsr GetDefault()
        {
            FctUsr oFctUsr =
                new FctUsr();

            oFctUsr.OwnerName = "Undefined!";

            return oFctUsr;
        }

    }
}