﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace neew.Models
{
    public class BaseEntityWithIsActive : BaseEntity
    {
        public BaseEntityWithIsActive()
        {
        }

        public bool IsActive { get; set; }
    }
}