﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace neew.Models
{
    public class User : Date
    {

        public User()
        {

        }

        public User(string uname, string fname, string lname, string email, string pass, string phone, string city)
        {
            this.UName = uname;
            this.FName = fname;
            this.LName = lname;
            this.Email = email;
            this.Pass = pass;
            this.Phone = phone;
            this.City = city;
        }


        [System.ComponentModel.DataAnnotations.Key]
        [System.ComponentModel.DataAnnotations.Required]
        public int ID { get; set; }

        [Required(ErrorMessage = "لطفا نام کاربری خود را وارد کنید")]
        public string UName { get; set; }

        [Required(ErrorMessage = "لطفا نام خود را وارد کنید")]
        public string FName { get; set; }

        [Required(ErrorMessage = "لطفا نام خانوادگی خود را وارد کنید")]
        public string LName { get; set; }


        [Required(ErrorMessage = "لطفا ایمیل خود را وارد کنید")]
        [RegularExpression(".+\\@.+\\..+",
            ErrorMessage = "لطفا ایمیل را صحیح وارد کنید")]
        public string Email { get; set; }


        [Required(ErrorMessage = "لطفا پسورد را وارد کنید")]
        //[StringLength(8, MinimumLength = 4, ErrorMessage = "لطفا پسورد را صحیح وارد کنید")]
        public string Pass { get; set; }


        [RegularExpression("^09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}$",
           ErrorMessage = "لطفا  تلفن را صحیح وارد کنید")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "لطفا استان خود را وارد کنید")]
        public string City { get; set; }





        public static User GetDefault()
        {
            User ouser =
                new User();

            ouser.UName = "Undefined!";

            return ouser;
        }
    }
}